const express = require("express");
const router = express.Router();

const userRouter = require("./userRouter");
const movieRouter = require("./movieRouter");
const functionRouter = require("./functionRouter");
const favoriteRouter = require("./favoriteRouter");

router.use("/user", userRouter);
router.use("/movie", movieRouter);
router.use("/function", functionRouter);
router.use("/favorite", favoriteRouter);

module.exports = router;