const express = require("express");
const router = express.Router();
const favoriteController = require("../controllers/favoriteController");

router.use("/", favoriteController);

module.exports = router;
