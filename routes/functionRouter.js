const express = require("express");
const router = express.Router();
const functionController = require("../controllers/functionController");

router.use("/", functionController);

module.exports = router;
