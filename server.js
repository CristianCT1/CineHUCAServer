const express = require('express');
const Mongoose  = require('mongoose');
const cors = require('cors');
const bodyParser = require('body-parser');
const dbConfig = require('./database/config');
const passport = require('passport');

const morgan = require('morgan');
const router = require('./routes');

// Connecting mongoDB Database
Mongoose.Promise = global.Promise;
Mongoose.connect(dbConfig.db, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(() => {
    console.log('Database sucessfully connected!');
}, error => {
    console.log('Could not connect to database : ' + error)
});

const app = express();


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(cors());
app.use(morgan('dev'));

// PORT
const port = process.env.PORT || 4000;
app.listen(port, () => {
    console.log('Connected to port ' + port)
});

//Routers
app.use('/api', router);

app.use(function (err, req, res, next) {
    console.error(err.message);
    if (!err.statusCode) err.statusCode = 500;
    res.status
});