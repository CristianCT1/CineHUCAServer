const mongoose = require("mongoose");
const { Schema } = mongoose;

const FavoriteSchema = new Schema({
    id_pelicula: { type: String, required: true },
    id_cliente: { type: String, required: true }
});

module.exports = mongoose.model("Favoritos", FavoriteSchema);