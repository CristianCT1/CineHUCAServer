const mongoose = require("mongoose");
const { Schema } = mongoose;

const ClientSchema = new Schema({
    nombre: { type: String, required: true },
    apellido: { type: String },
    correo: { type: String, required: true },
    password: { type: String, required: true },
    telefono: { type: Number },
    fecha_nacimiento: { type: Date, required: true }
});

module.exports = mongoose.model("Clientes", ClientSchema);