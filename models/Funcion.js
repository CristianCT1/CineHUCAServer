const mongoose = require("mongoose");
const { Schema } = mongoose;

const FunctionSchema = new Schema({

    id_pelicula: { type: String, required: true },
    numero_sala: { type: String, required: true },
    fecha: { type: Date, default: Date.now }
});

module.exports = mongoose.model("Funciones", FunctionSchema);