const mongoose = require("mongoose");
const { Schema } = mongoose;

const MovieSchema = Schema({
    nombre: { type: String, required: true },
    duracion: { type: Number, required: true },
    genero: { type: String, required: true },
    descripcion: { type: String },
    url_imagen: { type: String, required: true },
    url_cover: { type: String },
    url_trailer: { type: String }
});

module.exports = mongoose.model("Peliculas", MovieSchema);