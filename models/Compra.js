const mongoose = require("mongoose");
const { Schema } = mongoose;

const PurchaseSchema = new Schema({
    id_asiento: { type: String, required: true },
    id_cliente: { type: String, required: true },
});

module.exports = mongoose.model("Compras", PurchaseSchema);