const mongoose = require("mongoose");
const { Schema } = mongoose;

const SeatSchema = new Schema({
    numero_funcion: { type: String, required: true },
    numero_asiento: { type: Number, required: true },
    disponible: { type: Boolean, required: true, default: false }
});

module.exports = mongoose.model("Asientos", SeatSchema);