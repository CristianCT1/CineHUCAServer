const mongoose = require("mongoose");
const { Schema } = mongoose;

const RoomSchema = new Schema({
    asientos: { type: Number, required: true }
});

module.exports = mongoose.model("Salas", RoomSchema);