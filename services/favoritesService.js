const Favorite = require("../models/Favorito");
const MovieService = require("./movieService");

module.exports = {
    list: async (req, res) => {
        const result = await Favorite.find({ id_cliente: req.body.id_cliente });
        const movies = result.map((movie) => MovieService.getMovie(movie.id_pelicula));
        res.json(movies);
    }
}