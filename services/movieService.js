const Movie = require("../models/Pelicula");

module.exports = { 
    list: async (req, res) => {
        const result = await Movie.find();
        res.json(result);
    },

    getMovie: async(req, res) => {
        const result = await Movie.findById(req.body.id_movie);
        res.json(result);
    },
    
    getMovieByGender: async(req, res) => {
        const result = await Movie.find({ genero: req.body.gender })
        res.json(result);
    },

    newMoview: async(req, res) => {
        console.log(req.body);
        Movie.create(
            {
                nombre: req.body.name,
                duracion: req.body.duration,
                genero: req.body.genre,
                descripcion: req.body.description,
                url_imagen: req.body.URLImage,
                url_cover: req.body.URLCover,
                url_trailer: req.body.URLTrailer
            }, (err, movie) => {
                if(err){
                    res.status(500).send(err);
                } else {
                    res.status(200).json(movie);
                }
            }
        )
    }
 }