const Function = require("../models/Funcion");
const Seat = require("../models/Asiento");
const Movie = require("../models/Pelicula");

module.exports = {
    list: async (req, res) => {
        const result = await Function.find();
        res.json(result);
    },

    getMovieFunctions: async (req, res) => {
        console.log(req.params);
        const result = await Function.find({ id_pelicula: req.params.idPelicula });
        res.json(result);
    },

    listFunctions: async (req, res,) => {
        const result = await Function.find({ id_pelicula: req.params.idMovie });
        const listResults = result.map((functionMovie) => {
            return {...result, asientos: Seat.find(functionMovie.numero_funcion)};
        });
        
        console.log(result);

        if (Promise.resolve(result) == []){
            res.send("Is empty");
        } else {
            res.json({ Movie: Movie.findById(result[0].id_pelicula), functions: listResults});
        }
    }
}
