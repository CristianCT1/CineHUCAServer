const User = require("../models/Cliente");

module.exports = {
    getUser: async(req, res) => {
        const result = await User.find({ correo: req.body.correo });
        res.json(result);
    },

    recoverPasswordUser: async (req, res) => {
        
    },

    updateUser: async (req, res) => {

    },

    newUser: async (req, res) => {
        if(User.find({ correo: req.body.correo })){
            res.send("El correo ya se encuentra reguistrado");
        }
    }
}