const express = require("express");
const router = express.Router();
const userService = require("../services/userService");

router.get("get", userService.getUser);
router.get("recover-password", userService.recoverPasswordUser);
router.put("update-user", userService.updateUser);
router.post("new-user", userService.newUser)
  
module.exports = router; 