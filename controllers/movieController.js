const express = require("express");
const router = express.Router();
const movieService = require("../services/movieService");

router.get("/list", movieService.list);
router.post("/add", movieService.newMoview);

module.exports = router;
