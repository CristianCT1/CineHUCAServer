const express = require("express");
const router = express.Router();
const functionService = require("../services/functionService");

router.get("/list", functionService.list);
router.get("/movie-functions/:idPelicula", functionService.getMovieFunctions);
router.get("/details/:idMovie", functionService.listFunctions);

module.exports = router;
