const express = require("express");
const router = express.Router();
const favoriteService = require("../services/favoritesService");

router.get("/", favoriteService.list);

module.exports = router;
